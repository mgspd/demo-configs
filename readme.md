Demo Config Server Repository 
==============================

##Setup for local development
1. Create new git branch and name it **<your_windows_user_name>_config**
2. Replace configuration placeholders in all config files with values specific to your local environment
3. Do not push any changes (but commit, otherwise config server reloading mechanism will always replace your locally modified config with version commited in repository) - config server will pick changes from your local git repository   
(This is temporary, as you most probably don't have an account on bitbucket, thus wouldn't be able to push to remote anyway)